import { JsonPipe, NgClass } from '@angular/common';
import { Component, inject } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { startWith } from 'rxjs';
import { InputErrorComponent } from '../../shared/input-error.component';

export const ALPHA_NUMERIC_REGEX = /^\w+$/

@Component({
  selector: 'app-demo4-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    InputErrorComponent,
    NgClass
  ],
  template: `
    <form [formGroup]="form" (submit)="save()">
      <div>
      <input type="checkbox" formControlName="isCompany"> Is Company?
      </div>
      
      <input
        type="text" placeholder="company name" formControlName="name"
        [ngClass]="{'error': form.get('name')?.invalid}"
      >
      <app-input-error [errors]="form.get('name')?.errors"></app-input-error>
      <hr>

      <input type="text" placeholder="your vat" formControlName="vat">
      <app-input-error [errors]="form.get('vat')?.errors"></app-input-error>
      
      <hr>
      <button type="submit" [disabled]="form.invalid">SAVE</button>
    </form>

    <button (click)="reset()">reset</button>

    <pre>Value: {{ form.value | json }}</pre>
    <pre>Dirty: {{ form.dirty | json }}</pre>
    <pre>Touched: {{ form.touched | json }}</pre>
    <pre>VAT IS ENABLED: {{ form.get('isCompany')?.value | json }}</pre>
    
    <!--<vat-compo [enabled]="form.get('isCompany')?.value"></vat-compo>-->
    <!--<vat-compo [enabled]="isVatValid$ | async"></vat-compo>-->
  `,
  styles: ``
})
export default class Demo4FormComponent {
  fb = inject(FormBuilder);
  form = this.fb.nonNullable.group({
    name: ['', [Validators.required]],
    vat: ['', [Validators.required, alphaNumericValidator]],
    isCompany: false
  })

  isVatValid$ = this.form.get('isCompany')?.valueChanges

  constructor() {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        console.log(isCompany)
        if (isCompany) {
          this.form.get('vat')?.enable()
        } else
          this.form.get('vat')?.disable()
      })

    this.form.get('isCompany')?.setValue(false)
  }

  save() {
    console.log('submit', this.form.value, this.form.getRawValue())
  }

  reset() {
    this.form.reset()
  }
}

function alphaNumericValidator(c: AbstractControl) {
  return (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) ?
    { alphaNumeric: true } : null
}
