import { JsonPipe, NgClass } from '@angular/common';
import { Component, Inject, inject, Injectable, signal } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors, ValidatorFn,
  Validators
} from '@angular/forms';
import { startWith } from 'rxjs';
import { InputErrorComponent } from '../../shared/input-error.component';

export const ALPHA_NUMERIC_REGEX = /^\w+$/

@Component({
  selector: 'app-demo5-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    InputErrorComponent,
    NgClass
  ],
  template: `
    <form [formGroup]="form" (submit)="save()">

      <div>
        <input type="checkbox" formControlName="isCompany"> Is Company?
      </div>

      <input
        type="text" placeholder="company name" formControlName="name"
        [ngClass]="{'error': form.get('name')?.invalid}"
      >
      <hr>

      <input type="text" [placeholder]="form.get('isCompany')?.value ? 'your vat' : 'your CF'" formControlName="vat">
      {{ form.get('vat')?.errors | json }}

      <hr>
      <button type="submit" [disabled]="form.invalid">SAVE</button>
    </form>
    <pre>{{ form.value | json }}</pre>

  `,
  styles: ``
})
export default class Demo5FormComponent {
  fb = inject(FormBuilder);
  myService = inject(MyService)

  form = this.fb.nonNullable.group({
    name: ['', [Validators.required]],
    vat: ['', [exactValidator(11)]],
    isCompany: false
  })

  // esempio in cui creiamo il form in un service (non usato in questa demo)
  form2 = this.myService.form;


  isVatValid$ = this.form.get('isCompany')?.valueChanges

  constructor() {
    this.form.get('isCompany')?.valueChanges
      .subscribe(isCompany => {
        console.log(isCompany)
        if (isCompany) {
          this.form.get('vat')?.setValidators([exactValidator(11, true)])
        } else {
          this.form.get('vat')?.setValidators([exactValidator(16)])
        }
        this.form.get('vat')?.updateValueAndValidity();

      })

    this.form.get('isCompany')?.setValue(false)
  }

  save() {
    console.log('submit', this.form.value, this.form.getRawValue())
  }

  reset() {
    this.form.reset()
  }
}

// form control validator sync
export function exactValidator(
  exactLength: number = 11, required = false
): ValidatorFn {
  return (c: AbstractControl): ValidationErrors | null => {
    if (required && !c.value) {
      return { requiredd: true}
    }
    if (c.value && c.value.length !== exactLength) {
      return { exact: true }
    }
    return null;
  }
}

/**
 * Esempio Service per i form
 */
@Injectable({ providedIn: 'root'})
export class MyService {
  fb = inject(FormBuilder);
  regexDict = signal({});

  form = this.fb.nonNullable.group({
    name: ['', [Validators.required]],
    vat: ['', [exactValidator(11)]],
    isCompany: false
  })

  getConfig() {
    // ... http
    // popoli le tue proprietà

  }
}

// validators-regex.ts
export const REGET_URL = '....'
