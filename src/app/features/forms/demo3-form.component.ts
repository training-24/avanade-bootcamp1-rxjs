import { JsonPipe, NgClass, NgIf } from '@angular/common';
import { Component, computed, effect, signal } from '@angular/core';
import { toObservable } from '@angular/core/rxjs-interop';
import {
  AbstractControl,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { InputErrorComponent } from '../../shared/input-error.component';

const REGEX_URL = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/

@Component({
  selector: 'app-demo3-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    FormsModule,
    NgIf,
    JsonPipe,
    NgClass,
    InputErrorComponent
  ],
  template: `
    <div>
      <div>Name</div>
      <input 
        type="text" [formControl]="input"
        [ngClass]="{'error': input.invalid}"
      >
      <app-input-error [errors]="input.errors" />

      
      @if(input.errors?.['url']) {
        <div> url sbagliato!</div>
      }
    </div>
    
    <hr>
    
    
    <div>value: {{ input.value | json}}</div>
    <div>dirty: {{ input.dirty | json}}</div>
    <div>touched: {{ input.touched | json}}</div>
    <div>errors: {{ input.errors | json}}</div>
    <div>valid: {{ input.valid | json}}</div>
    
    <button (click)="reset()">Reset</button>
    
    <hr>
  `,
  styles: `
  
    .error {
      background-color: red;
    }
  `
})
export default class Demo3FormComponent {
  // input = new FormControl('', { nonNullable: true })
  //input = new FormControl('', [])
  inputRole = new FormControl<'admin' | 'moderator'>('admin')
  input = new FormControl('', {
    nonNullable: true,
    validators: [
      Validators.required,
      // Validators.minLength(3),
      // Validators.pattern(REGEX_URL),
      customValidator
    ]
  })
  constructor() {
    this.inputRole.setValue('admin')
  }

  reset() {
    this.input.reset()
  }
}

export function customValidator(c: AbstractControl): ValidationErrors | null {
  if (c.value.match(REGEX_URL)) {
    return null
  }
  return { url: true }
}
