import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ColorPickerComponent } from '../../shared/forms/color-picker.component';
import { MyInputComponent } from '../../shared/forms/my-input.component';

@Component({
  selector: 'app-demo7-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    ColorPickerComponent,
    JsonPipe,
    MyInputComponent
  ],
  template: `
    <p>
      demo7-form works!
    </p>

    <form [formGroup]="form">
      <app-my-input
        label="Name:" 
        formControlName="name"
        alphaNumeric
      />
      
      <hr>

      <app-color-picker formControlName="color"/>
      {{ form.get('color')?.errors | json }}
      
    </form>


    <hr>
    <h1>FORM INFO</h1>
    valid {{ form.valid | json }}
    {{ form.value | json }}
    {{ form.dirty | json }}
    {{ form.touched | json }}

  `,
  styles: ``
})
export default  class Demo7FormComponent {
  fb = inject(FormBuilder)

  form = this.fb.nonNullable.group({
    name: ['', [Validators.required]],
    color: ['', Validators.required],
  })

  constructor() {
   /* setTimeout(() => {
      this.form.patchValue({
        color: 'blue',
        name: 'PIppo'
      })
    }, 1000)*/
  }
}
