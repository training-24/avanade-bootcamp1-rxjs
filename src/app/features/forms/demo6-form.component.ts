import { AsyncPipe, JsonPipe, NgClass } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, computed, inject, Injectable, signal } from '@angular/core';
import {
  AbstractControl, AsyncValidatorFn,
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors, ValidatorFn,
  Validators
} from '@angular/forms';
import { debounce, debounceTime, first, map, mergeMap, Observable, of, startWith, tap, timeout, timer } from 'rxjs';

@Component({
  selector: 'app-demo6-form',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe,
    NgClass,
    AsyncPipe
  ],
  template: `

    <form [formGroup]="form">
      <pre>{{ form.get('username')?.errors | json }}</pre>
      @if(form.get('username')?.pending ) {
        <div>💩loading</div>
      }
      <input type="text" placeholder="username" formControlName="username">
      <hr>

      <div formGroupName="passwords">
        <pre>{{ form.get('passwords')?.errors | json }}</pre>
        <pre>{{ form.get('passwords.password2')?.errors | json }}</pre>
        <input type="text" placeholder="pass1" formControlName="password1">
        <input type="text" placeholder="pass2" formControlName="password2">
      </div>
      <hr>
      

      <div formGroupName="anagrafica" [ngClass]="{valid: isAnagraficaValid | async}">
        <h1>
          @if (isAnagraficaValid | async) {
            ✅
          }
          Anagrafica
        </h1>
        <input type="text" placeholder="name" formControlName="name">
        <input type="text" placeholder="surname" formControlName="surname">
      </div>
      <hr>
      <button [disabled]="form.invalid || form.pending">SAVE</button>
    </form>

    <pre>{{ form.value| json }}</pre>
  `,
  styles: `
    .valid {
      border: 2px solid green;
      padding: 10px
    }
  `
})
export default class Demo6FormComponent {
  fb = inject(FormBuilder)

  form = this.fb.nonNullable.group({
    username: ['', [Validators.required], [checkUsername()]],
    passwords: this.fb.group(
      {
        password1: ['ABC', [Validators.required, Validators.minLength(3)]],
        password2: ['BCD', [Validators.required, Validators.minLength(3)]],
      }, {
        validators: passwordMatch()
      }
    ),
    anagrafica: this.fb.nonNullable.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      country: '',
      zip: ''
    })
  })



  isAnagraficaValid =  this.form.get('anagrafica')?.valueChanges
    .pipe(
      startWith(null),
      map(() => !!this.form.get('anagrafica')?.valid )
    )

  constructor() {
    const res = {
      username: 'pippo',
      anagrafica: {
        name: 'Fab',
        surname: 'xyz'
      }
    }
    this.form.patchValue(res)


  }
}

// group validator
export const passwordMatch = () => {
  return (g: FormGroup): ValidationErrors | null => {
    const p1 = g.get('password1');
    const p2 = g.get('password2');

    const passMatch = p1?.value === p2?.value;

    if (passMatch)
      return null;
    return {  invalidPass: true }
  }
}

// form control validator async
export function checkUsername(): AsyncValidatorFn {
  const http = inject(HttpClient)

  // senza debounce
  /*return (c: AbstractControl) => {
    return http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)
      .pipe(
        map(res => res.length ? { invalidUsername: true } : null)
      )
  }*/

  return (c: AbstractControl) => {
    return timer(1000)
      .pipe(
        first(),
        mergeMap(() => http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)
          .pipe(
            map(res => res.length ? { invalidUsername: true } : null)
          ))
      )
  }


}

@Injectable({providedIn: 'root'})
export class UserValidator {
  http = inject(HttpClient)


  checkUsername(): AsyncValidatorFn {
    return (c: AbstractControl) => {
      return this.http.get<any[]>(`https://jsonplaceholder.typicode.com/users?username=${c.value}`)
        .pipe(
          map(res => res.length ? { invalidUsername: true } : null)
        )
    }
  }

}
