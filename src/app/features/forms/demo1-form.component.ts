import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, computed, ElementRef, inject, signal, ViewChild } from '@angular/core';
import { catchError, debounceTime, filter, fromEvent, map, mergeMap, of } from 'rxjs';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-demo1-form',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf
  ],
  template: `
    <h1>Form RXJS</h1>

    <input type="text" #myinput>

    <div *ngIf="meteo() as meteo">
      {{ meteo.main.temp | json }}
    </div>
    
    @if(temperature(); as meteo) {
      <div style="padding: 20px; background-color: red">
        {{ temperature() }}
      </div>
    }
  `,
  styles: ``
})
export  default class Demo1FormComponent implements AfterViewInit {
  @ViewChild('myinput') input: ElementRef<HTMLInputElement> | undefined
  http = inject(HttpClient)
  meteo = signal<Meteo | null>(null)
  temperature = computed(() => this.meteo()?.main.temp)

  ngAfterViewInit() {
    if (this.input) {
      fromEvent(this.input.nativeElement, 'input')
        .pipe(
          map(ev => (ev.target as HTMLInputElement).value),
          filter(text => text.length > 2),
          debounceTime(1000),
          mergeMap(
            (text) => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
              .pipe(
                catchError(() => of(null))
              )
          ),
        )
        .subscribe(meteo => {
          this.meteo.set(meteo)
        })
    }


  }
}
