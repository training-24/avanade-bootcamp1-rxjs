import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, computed, ElementRef, inject, signal, ViewChild } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { catchError, debounceTime, filter, fromEvent, map, mergeMap, of } from 'rxjs';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-demo2-form',
  standalone: true,
  imports: [
    JsonPipe,
    NgIf,
    ReactiveFormsModule
  ],
  template: `
    <h1>Form Reactive</h1>

    <input type="text" [formControl]="input">

    <div *ngIf="meteo() as meteo">
      {{ meteo.main.temp | json }}
    </div>

    @if(temperature(); as meteo) {
      <div style="padding: 20px; background-color: red">
        {{ temperature() }}
      </div>
    }
    
  `,
  styles: ``
})
export  default class Demo2FormComponent implements AfterViewInit {
  // @ViewChild('myinput') input: ElementRef<HTMLInputElement> | undefined
  input = new FormControl
  http = inject(HttpClient)
  meteo = signal<Meteo | null>(null)
  temperature = computed(() => this.meteo()?.main.temp)

  ngAfterViewInit() {
    if (this.input) {
      this.input.valueChanges
        .pipe(
          filter(text => text.length > 2),
          debounceTime(1000),
          mergeMap(
            (text) => this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
              .pipe(
                catchError(() => of(null))
              )
          ),
        )
        .subscribe(meteo => {
          this.meteo.set(meteo)
        })
    }


  }
}
