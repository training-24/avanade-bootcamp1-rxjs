import { log } from '@angular-devkit/build-angular/src/builders/ssr-dev-server';
import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, ElementRef, inject, ViewChild } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import {
  combineLatest, concatMap,
  debounceTime,
  delay, exhaustMap,
  filter, fromEvent,
  interval,
  map,
  mergeAll,
  mergeMap, of,
  subscribeOn,
  switchMap, tap, toArray
} from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';


const API = 'https://jsonplaceholder.typicode.com/users'
@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe
  ],
  template: `
    <input type="text" [formControl]="input1" >
    <select [formControl]="select">
      <option value="">Select</option>
      <option value="A">A</option>
      <option value="B">B</option>
    </select>
    <button #btn>CLICK</button>
    <pre>{{users | json}}</pre>

  `,
  styles: ``
})
export default class Demo4Component {
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>
  input1 = new FormControl('', { nonNullable: true })
  select = new FormControl('')
  http = inject(HttpClient)
  users: User[] = []

  ngAfterViewInit() {
    fromEvent(this.button.nativeElement, 'click')
      .pipe(
        exhaustMap(() => {
          return this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users/${1}`)
            .pipe(delay(1000))
        })
      )
      .subscribe(() => {
        console.log('qui')
      })

    of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
      .pipe(
        exhaustMap(
          index => this.http.get<User[]>(`https://jsonplaceholder.typicode.com/users/${index}`)
        ),
        toArray()
      )
      .subscribe({
        next: result => {
          console.log(result)
          // this.users = result;
        },
        error: () => {
          console.log('ahia')
        }
      })



    combineLatest([
      this.input1.valueChanges,
      this.select.valueChanges
    ])
      .pipe(
        // destructuring
        mergeMap(([name, gender]) => this.http.get<User[]>(`${API}?q=${name}&x=${gender}`)
          .pipe(
            delay(2000)
          )
      ),
    )
      .subscribe(result => {
        console.log(result)
        this.users = result;
      })
  }
}
