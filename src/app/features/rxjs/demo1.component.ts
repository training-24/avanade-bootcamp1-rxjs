import { AsyncPipe, JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, signal } from '@angular/core';
import { delay, filter, interval, map, share, shareReplay } from 'rxjs';
import { ThemeService } from '../../core/theme.service';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo1',
  standalone: true,
  imports: [
    JsonPipe,
    AsyncPipe,
    NgIf
  ],
  template: `

    <div *ngIf="(user$ | async) as user">
      <h1>{{ user.name }}</h1>
      <h1>{{ user.phone }}</h1>
    </div>
    @if (user$ | async; as user) {
      <h1>{{ user.name }}</h1>
      <h1>{{ user.phone }}</h1>
    }

    <div>{{ (users$ | async)?.length }} users</div>
    @for (user of (users$ | async); track user.id) {
      <li>{{ user.name }}</li>
    }

    <h1>{{ (user$ | async)?.website }}</h1>
  `,
})
export default class Demo1Component {
  http = inject(HttpClient);
  themeSrv = inject(ThemeService)

  users$ = this.http
    .get<User[]>('https://jsonplaceholder.typicode.com/users')
    .pipe(
      share()
      // map(users => users.map(u => u.name))
    )

  user$ = this.http
    .get<User>('https://jsonplaceholder.typicode.com/users/1')
    .pipe(
      shareReplay(1)
    )

  constructor() {
    this.user$.subscribe(res => {
      console.log(res)
    })
    setTimeout(() => {
      console.log('qui')
      this.user$.subscribe(res => {
        console.log('->', res)
      })
    }, 2000)

  }
}
