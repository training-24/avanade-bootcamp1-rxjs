import { AsyncPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, forkJoin, interval, share, take, timer } from 'rxjs';
import { Post } from '../../model/post';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo3',
  standalone: true,
  imports: [
    NgIf,
    AsyncPipe,
    ReactiveFormsModule
  ],
  template: `
    <h1>{{ (data$ | async)?.timer }}</h1>
    <h1>{{ (data$ | async)?.text }}</h1>
    
    <input type="text" [formControl]="input">

    @for (user of (data$ | async)?.users; track user.id) {
      <li>{{ user.name }}</li>
    }
    @for (post of (data$ | async)?.posts; track post.id) {
      <li>{{ post.title }}</li>
    }
    
  `,
  styles: ``
})
export default  class Demo3Component {
  http = inject(HttpClient)
  input = new FormControl('abc')


  data$ = combineLatest({
    users: this.http.get<User[]>('https://jsonplaceholder.typicode.com/users'),
    posts: this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts'),
    timer: interval(1000),
    text: this.input.valueChanges
  })
    .pipe(
     // share()
    )



}
