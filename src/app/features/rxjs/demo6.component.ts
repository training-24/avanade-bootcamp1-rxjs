import { AsyncPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { BehaviorSubject, mergeMap, Subject } from 'rxjs';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [
    AsyncPipe,
    ReactiveFormsModule
  ],
  template: `
    <p>
      demo6 works!
    </p>

    <select [formControl]="select">
      <option value="dark">DARK</option>
      <option value="light">LIGHT</option>
    </select>

    {{ themeSrv.theme$ | async }}
  `,
  styles: ``
})
export default class Demo6Component {
  select = new FormControl<'dark' | 'light'>('dark', { nonNullable: true })
  themeSrv = inject(ThemeService)

  constructor() {

    this.select.valueChanges
      .subscribe(choice => {
        this.themeSrv.changeTheme(choice)
      })



  }
}
