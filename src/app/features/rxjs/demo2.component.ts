import { AsyncPipe } from '@angular/common';
import { Component } from '@angular/core';
import { interval, share, shareReplay } from 'rxjs';

@Component({
  selector: 'app-demo2',
  standalone: true,
  imports: [
    AsyncPipe
  ],
  template: `
    <h1>{{timer$ | async}}</h1>
    
    <button (click)="startTimer()">Start</button>
  `,
  styles: ``
})
export default class Demo2Component {
  timer$ = interval(1000)
    .pipe(
      shareReplay(3)
    )

  constructor() {
    //const sub = this.timer$.subscribe()

   /* setTimeout(() => {
      sub.unsubscribe()
    }, 1000)*/
  }

  startTimer() {
    this.timer$.subscribe(console.log)
  }
}
