import { JsonPipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject } from '@angular/core';
import { ActivatedRoute, RouterLink } from '@angular/router';
import { catchError, forkJoin, map, mergeMap, of, switchMap, tap } from 'rxjs';
import { Post } from '../../model/post';
import { Todo } from '../../model/todo';
import { User } from '../../model/user';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [
    RouterLink,
    JsonPipe
  ],
  template: `
    <p>
      demo5 works!
    </p>
    
    <button routerLink="../26">26</button>
    <button routerLink="../44">44</button>
    <button routerLink="../111">111</button>
    
    <pre>{{data | json}}</pre>
  `,
  styles: ``
})
export default class Demo5Component {
  http = inject(HttpClient)
  route = inject(ActivatedRoute)
  data: any ;
  post$ = (postId: number) => this.http.get<Post>(`https://jsonplaceholder.typicode.com/posts/${postId}`)
    .pipe(
      catchError(e => of(null)),

    )
  user$ = (post: Post | null) => this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${post?.userId}`)
    .pipe(
      catchError(e => of(null)),
      map(user => ({ user, post })),
    )

  constructor() {
    this.route.params
      .pipe(
        switchMap(params => this.post$(params['postId'])),
        switchMap(post => this.user$(post)),
      )
      .subscribe(obj => {
        console.log(obj)
        // this.data = obj
      })


    return
    this.http.get<Post[]>('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        map(posts => posts[23].userId),
        switchMap(userID => forkJoin({
          profile: this.http.get<User>(`https://jsonplaceholder.typicode.com/users/${userID}`),
          todos: this.http.get<Todo[]>(`https://jsonplaceholder.typicode.com/todos?userId=${userID}`),
        })),
      )
      .subscribe(data => {
        console.log(data)
      })
  }
}
