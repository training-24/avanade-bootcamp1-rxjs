import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';


@Component({
  selector: 'app-input-error',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `
    <div>
    {{msg()}}

    @for (error of errorsArray(); track $index) {
      ...{{dict[error]}}      
    }
    </div>
  `,
  styles: ``
})
export class InputErrorComponent {
  dict: any = {
    'url': 'wrong url!',
    'required': 'obbligatorio',
    'alphaNumeric': 'non sono permessi simboli',
  }
  errors = input<ValidationErrors | null>();

  errorsArray = computed( () => {
    return Object.keys(this.errors() || []);
  })

  msg = computed(() => {
    const err = this.errors()
    if (err && err['required']) {
      return 'required field'
    }
    if (err && err['minlength']) {
      return 'too short. Min 3 chars'
    }
    if (err && err['pattern']) {
      return 'Wrong url format'
    }
 if (err && err['url']) {
      return 'Wrong url format'
    }

    return ''
  })
}
