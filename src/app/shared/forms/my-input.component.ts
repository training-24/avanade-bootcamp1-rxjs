import { JsonPipe } from '@angular/common';
import { booleanAttribute, Component, inject, Injector, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR, NgControl,
  ReactiveFormsModule, ValidationErrors, Validator,
  Validators
} from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-my-input',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    JsonPipe
  ],
  template: `
    <div>
      <div>{{ label }}</div>
      <input 
        type="text"
         [formControl]="input"
        (blur)="touchedFn()"
      >
    </div>
    @if(control.errors) {
      @if(control.errors['required']){
        <div>Campo Obbligatorio</div>
      }
      @if(control.errors['alphaNumeric']){
        <div>No symbol allowed</div>
      }
    }
    
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: MyInputComponent,
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: MyInputComponent,
      multi: true
    }
  ]
})
export class MyInputComponent implements ControlValueAccessor, Validator {
  @Input() label: string = ''
  @Input({ transform: booleanAttribute }) alphaNumeric: boolean = false;
  touchedFn!: () => void
  subject = new Subject();
  // ngControl = inject(NgControl);
  inj = inject(Injector)
  control!: NgControl

  ngOnInit() {
    this.control = this.inj.get(NgControl)
  }


  input = new FormControl('', {
    nonNullable: true,
  });

  writeValue(value: string): void {
    this.input.setValue(value)
  }
  registerOnChange(changeFn: any): void {
    this.input.valueChanges
      .pipe(
        takeUntil(this.subject)
      )
      .subscribe(changeFn)
  }
  registerOnTouched(fn: any): void {
    this.touchedFn = fn;
  }

  ngOnDestroy() {
    this.subject.next(null);
    this.subject.complete();
  }

  validate(c: AbstractControl): ValidationErrors | null {
    if (this.alphaNumeric) return this.validateAlphaNumeric(c);

    return null;
  }

  validateAlphaNumeric(c: AbstractControl) {
    if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
      return { alphaNumeric: true }
    }
    return null;
  }
}
const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;
