import { NgClass } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Component, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-color-picker',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    NgClass
  ],
  template: `
    {{value}}
    <div style="display: flex">
    @for(c of colors; track $index) {
      <div 
        class="cell"
        [ngClass]="{ active: c === value}"
        [style.background-color]="c"
        (click)="changeColor(c)"
      ></div>
    }
    </div>
  `,
  styles: `
    .cell {
      width: 30px;
      height: 30px;
    }
    .active {
      border: 2px solid black
    }
  `,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: ColorPickerComponent
    }
  ]
})
export class ColorPickerComponent implements ControlValueAccessor{
  value: string | undefined;
  @Input() colors = ['red', 'yellow', 'blue']

  changeFn!: (color: string) => void;
  touchedFn!: () => void

  writeValue(value: string): void {
    this.value = value;
  }
  registerOnChange(fn: any): void {
    this.changeFn = fn;
  }
  registerOnTouched(fn: any): void {
    this.touchedFn = fn;
  }

  changeColor(color: string) {
    this.value = color;
    this.changeFn(color)
    this.touchedFn();
  }
}
