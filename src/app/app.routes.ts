import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: 'demo1', loadComponent: () => import('./features/rxjs/demo1.component')},
  { path: 'demo2', loadComponent: () => import('./features/rxjs/demo2.component')},
  { path: 'demo3', loadComponent: () => import('./features/rxjs/demo3.component')},
  { path: 'demo4', loadComponent: () => import('./features/rxjs/demo4.component')},
  { path: 'demo6', loadComponent: () => import('./features/rxjs/demo6.component')},
  { path: 'demo5/:postId', loadComponent: () => import('./features/rxjs/demo5.component')},

  { path: 'demo1form', loadComponent: () => import('./features/forms/demo1-form.component')},
  { path: 'demo2form', loadComponent: () => import('./features/forms/demo2-form.component')},
  { path: 'demo3form', loadComponent: () => import('./features/forms/demo3-form.component')},
  { path: 'demo4form', loadComponent: () => import('./features/forms/demo4-form.component')},
  { path: 'demo5form', loadComponent: () => import('./features/forms/demo5-form.component')},
  { path: 'demo6form', loadComponent: () => import('./features/forms/demo6-form.component')},
  { path: 'demo7form', loadComponent: () => import('./features/forms/demo7-form.component')},
  { path: 'demo8form', loadComponent: () => import('./features/forms/demo8-form.component')},
  { path: 'demo9form', loadComponent: () => import('./features/forms/demo9-form.component')},
  { path: 'demo10form', loadComponent: () => import('./features/forms/demo10-form.component')},

  { path: '', redirectTo: 'demo1', pathMatch: 'full'},
  // { path: '**', redirectTo: 'demo1', pathMatch: 'full'}
];
