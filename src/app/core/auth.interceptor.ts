import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, interval, map, of, retry, tap, throwError } from 'rxjs';
import { ThemeService } from './theme.service';

export const authInterceptor: HttpInterceptorFn = (req, next) => {
  const themeService = inject(ThemeService)
  const router = inject(Router)

  let clone = req;
  if ( req.url.includes('jsonplaceholder')) {
    clone = req.clone({
      setHeaders: {
        Authorization: themeService.theme$.getValue()
      }
    })
  }

  return next(clone)
    .pipe(
      // tap((res) => console.log(res)),
      retry({ count: 2, delay: () => interval(200)}),
      catchError(err => {

        console.log('ERRORE!', err)
        switch (err.status) {
          case 401:
            break;

          default:
          case 403:
            // router.navigateByUrl('demo1')
            break;
        }
        return throwError(err)
      })
    )
};
