import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  theme$ = new BehaviorSubject<'dark' | 'light'>('dark')

  changeTheme(theme: any) {
    this.theme$.next(theme)
  }
}
