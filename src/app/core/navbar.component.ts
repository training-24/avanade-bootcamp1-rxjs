import { AsyncPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { ThemeService } from './theme.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [
    RouterLink,
    AsyncPipe
  ],
  template: `
    <div>
      RXJS
      <button routerLink="demo1">Demo1</button>
      <button routerLink="demo2">Demo2</button>
      <button routerLink="demo3">Demo3</button>
      <button routerLink="demo4">Demo4</button>
      <button routerLink="demo5/24">Demo5</button>
      <button routerLink="demo6">Demo6</button>
      {{ themeSrv.theme$ | async }}
    </div>
    <div>
      Forms
      <button routerLink="demo1form">Demo1</button>
      <button routerLink="demo2form">Demo2</button>
      <button routerLink="demo3form">Demo3</button>
      <button routerLink="demo4form">Demo4</button>
      <button routerLink="demo5form">Demo5</button>
      <button routerLink="demo6form">Demo6</button>
      <button routerLink="demo7form">Demo7</button>
      <button routerLink="demo8form">Demo8</button>
      <button routerLink="demo9form">Demo9</button>
      <button routerLink="demo10form">Demo10</button>
    </div>
  `,
  styles: ``
})
export class NavbarComponent {
  themeSrv = inject(ThemeService)
}
